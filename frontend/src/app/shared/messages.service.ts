import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageData } from './message.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: HttpClient) { }

  encodeMessage(messageData: MessageData) {
    return this.http.post<{[key:string]: string}>(environment.apiUrl + '/encode', messageData);
  }

  decodeMessage(messageData: MessageData) {
    return this.http.post<{[key:string]: string}>(environment.apiUrl + '/decode', messageData);
  }
}
