import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../shared/messages.service';
import { MessageData } from '../shared/message.model';


@Component({
  selector: 'app-app-vigenere',
  templateUrl: './app-vigenere.component.html',
  styleUrls: ['./app-vigenere.component.sass']
})
export class AppVigenereComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {}

  onEncode() {
    const messageData: MessageData = {
      password: this.form.value.password,
      message: this.form.value.decodedMessage
    };
    this.messagesService.encodeMessage(messageData).subscribe((encodedMessage) => {
      this.patchFormValue({
        encodedMessage: encodedMessage.encoded
      });
    });
  }

  onDecode() {
    const messageData: MessageData = {
      password: this.form.value.password,
      message: this.form.value.encodedMessage,
    };
    this.messagesService.decodeMessage(messageData).subscribe((decodedMessage) => {
      this.patchFormValue({
        decodedMessage: decodedMessage.decoded
      });
    });
  }

  patchFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.form.form.patchValue(value);
    });
  }
}
