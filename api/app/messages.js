const express = require('express');
const router = express.Router();
const {Cipher, Decipher} = require('caesar-salad').Vigenere;

router.post('/encode', (req, res) => {
  const message = {
    password: req.body.password,
    message: req.body.message
  };
  const encodedMessage = Cipher(message.password).crypt(message.message);
  return res.send({encoded: encodedMessage});
});

router.post('/decode', (req, res) => {
  const message = {
    password: req.body.password,
    message: req.body.message
  };
  const decodedMessage = Decipher(message.password).crypt(message.message);
  return res.send({decoded: decodedMessage});
});

module.exports = router;
